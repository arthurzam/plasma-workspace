# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2019, 2020, 2021, 2023, 2024 A S Alam <alam.yellow@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-08 00:39+0000\n"
"PO-Revision-Date: 2024-02-05 07:30-0600\n"
"Last-Translator: A S Alam <aalam@punlinux.org>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: kcm.cpp:70
#, kde-format
msgid "Toggle do not disturb"
msgstr "ਤੰਗ ਨਾ ਕਰੋ ਨੂੰ ਬਦਲੋ"

#: sourcesmodel.cpp:392
#, kde-format
msgid "Other Applications"
msgstr "ਹੋਰ ਐਪਲੀਕੇਸ਼ਨਾਂ"

#: ui/ApplicationConfiguration.qml:89
#, kde-format
msgid "Show popups"
msgstr "ਪੋਪਅੱਪ ਵੇਖਾਓ"

#: ui/ApplicationConfiguration.qml:103
#, kde-format
msgid "Show in do not disturb mode"
msgstr "ਤੰਗ ਨਾ ਕਰੋ ਢੰਗ ਵਿੱਚ ਵੇਖਾਓ"

#: ui/ApplicationConfiguration.qml:116 ui/main.qml:183
#, kde-format
msgid "Show in history"
msgstr "ਅਤੀਤ ਵਿੱਚ ਵੇਖਾਓ"

#: ui/ApplicationConfiguration.qml:127
#, kde-format
msgid "Show notification badges"
msgstr "ਨੋਟੀਫਿਕੇਸ਼ਨ ਬਿੱਲੇ ਵੇਖਾਓ"

#: ui/ApplicationConfiguration.qml:162
#, kde-format
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "ਈਵੈਟਾਂ ਦੀ ਸੰਰਚਨਾ"

#: ui/ApplicationConfiguration.qml:170
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr "ਇਹ ਐਪਲੀਕੇਸ਼ਨ ਹਰ-ਈਵੈਂਟ ਦੇ ਆਧਾਰ ਉੱਤੇ ਨੋਟੀਫਿਕੇਸ਼ਨਾਂ ਦੀ ਸੰਰਚਨਾ ਲਈ ਸਹਾਇਕ ਨਹੀਂ ਹੈ"

#: ui/ApplicationConfiguration.qml:264
#, kde-format
msgid "Show a message in a pop-up"
msgstr "ਪੌਪ-ਅੱਪ ਵਿੱਚ ਸੁਨੇਹਾ ਵੇਖਾਓ"

#: ui/ApplicationConfiguration.qml:273
#, kde-format
msgid "Play a sound"
msgstr "ਆਵਾਜ਼ ਚਲਾਓ"

#: ui/main.qml:46
#, fuzzy, kde-format
#| msgid "Notifications"
msgctxt "@action:button Plasma-specific notifications"
msgid "System Notifications…"
msgstr "ਨੋਟੀਫਿਕੇਸ਼ਨ"

#: ui/main.qml:52
#, fuzzy, kde-format
#| msgid "Application Settings"
msgctxt "@action:button Application-specific notifications"
msgid "Application Settings…"
msgstr "ਐਪਲੀਕੇਸ਼ਨ ਸੈਟਿੰਗਾਂ"

#: ui/main.qml:77
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"ਨੋਟੀਫਿਕੇਸ਼ਨ ਵਿਖਾਉਣ ਲਈ ਚਾਹੀਦਾ 'ਨੋਟੀਫਿਕੇਸ਼ਨ' ਵਿਜੈਟ ਲੱਭਿਆ ਨਹੀਂ ਜਾ ਸਕਿਆ ਹੈ। ਯਕੀਨੀ ਬਣਾਓ ਕਿ "
"ਤੁਹਾਡੀ ਸਿਸਟਮ ਟਰੇ ਵਿੱਚ ਜਾਂ ਇਕੱਲੇ ਵਿਜੈਟ ਵਜੋਂ ਸਮਰੱਥ ਹੈ।"

#: ui/main.qml:88
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "ਇਸ ਵੇਲੇ ਨੋਟੀਫਿਕੇਸ਼ਨ ਪਲਾਜ਼ਮਾ ਦੀ ਬਜਾਏ '%1 %2' ਵਲੋਂ ਦਿੱਤੇ ਜਾ ਰਹੇ ਹਨ।"

#: ui/main.qml:92
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "ਇਸ ਵੇਲੇ ਨੋਟੀਫਿਕੇਸ਼ਨ ਪਲਾਜ਼ਮਾ ਵਲੋਂ ਨਹੀਂ ਦਿੱਤੇ ਜਾ ਰਹੇ ਹਨ।"

#: ui/main.qml:99
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "ਤੰਗ ਨਾ ਕਰੋ ਮੋਡ"

#: ui/main.qml:104
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "Enable automatically:"
msgstr "ਆਪਣੇ-ਆਪ ਸਮਰੱਥ ਕਰੋ:"

#: ui/main.qml:105
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "ਜਦੋਂ ਸਕਰੀਨਾਂ ਨੂੰ ਮਿੱਰਰ ਕੀਤਾ ਜਾਂਦਾ ਹੈ"

#: ui/main.qml:117
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "ਸਕਰੀਨ ਸਾਂਝੀ ਕਰਨ ਦੌਰਾਨ"

#: ui/main.qml:132
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Manually toggle with shortcut:"
msgstr "ਸ਼ਾਰਟਕੱਟ ਨਾਲ ਖੁਦ ਬਦਲੋ:"

#: ui/main.qml:139
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "ਦੇਖਣ ਦੀਆਂ ਸ਼ਰਤਾਂ"

#: ui/main.qml:144
#, kde-format
msgid "Critical notifications:"
msgstr "ਗੰਭੀਰ ਨੋਟੀਫਿਕੇਸ਼ਨ:"

#: ui/main.qml:145
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "ਤੰਗ ਨਾ ਕਰੋ ਢੰਗ ਵਿੱਚ ਵੇਖਾਓ"

#: ui/main.qml:157
#, kde-format
msgid "Normal notifications:"
msgstr "ਸਧਾਰਨ ਨੋਟੀਫਿਕੇਸ਼ਨ:"

#: ui/main.qml:158
#, kde-format
msgid "Show over full screen windows"
msgstr "ਪੂਰੀ ਸਕਰੀਨ ਵਿੰਡੋਆਂ ਉੱਤੇ ਵੇਖਾਓ"

#: ui/main.qml:170
#, kde-format
msgid "Low priority notifications:"
msgstr "ਘੱਟ ਤਰਜੀਹ ਨੋਟੀਫਿਕੇਸ਼ਨ:"

#: ui/main.qml:171
#, kde-format
msgid "Show popup"
msgstr "ਪੌਪਅੱਪ ਵੇਖਾਓ"

#: ui/main.qml:200
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "ਪੌਪਅੱਪ"

#: ui/main.qml:206
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "ਟਿਕਾਣਾ: "

#: ui/main.qml:207
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "ਨੋਟੀਫਿਕੇਸ਼ਨ ਆਈਕਾਨ ਦੇ ਨੇੜੇ"

#: ui/main.qml:244
#, kde-format
msgid "Choose Custom Position…"
msgstr "…ਕਸਟਮ ਸਥਿਤੀ ਚੁਣੋ"

#: ui/main.qml:253 ui/main.qml:269
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 ਸਕਿੰਟ"
msgstr[1] "%1 ਸਕਿੰਟ"

#: ui/main.qml:258
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "ਇਸ ਦੇ ਬਾਦ ਲੁਕਾਓ:"

#: ui/main.qml:281
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "ਵਧੀਕ ਫੀਡਬੈਂਕ"

#: ui/main.qml:287
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "ਨੋਟੀਫਿਕੇਸ਼ਨ ਵਿੱਚ ਵੇਖਾਓ"

#: ui/main.qml:288
#, kde-format
msgid "Application progress:"
msgstr "ਐਪਲੀਕੇਸ਼ਨ ਤਰੱਕੀ:"

#: ui/main.qml:302
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "ਤਰੱਕੀ ਦੇ ਦੌਰਾਨ ਪੋਪਅੱਪ ਰੱਖੋ"

#: ui/main.qml:315
#, kde-format
msgid "Notification badges:"
msgstr "ਨੋਟੀਫਿਕੇਸ਼ਨ ਬਿੱਲੇ:"

#: ui/main.qml:316
#, kde-format
msgid "Show in task manager"
msgstr "ਟਾਸਕ ਮੈਨੇਜਰ ਵਿੱਚ ਵੇਖਾਓ"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "ਪੌਪਅੱਪ ਸਥਿਤੀ"

#: ui/SourcesPage.qml:20
#, kde-format
msgid "Application Settings"
msgstr "ਐਪਲੀਕੇਸ਼ਨ ਸੈਟਿੰਗਾਂ"

#: ui/SourcesPage.qml:107
#, kde-format
msgid "Applications"
msgstr "ਐਪਲੀਕੇਸ਼ਨਾਂ"

#: ui/SourcesPage.qml:108
#, kde-format
msgid "System Services"
msgstr "ਸਿਸਟਮ ਸੇਵਾਵਾਂ"

#: ui/SourcesPage.qml:156
#, kde-format
msgid "No application or event matches your search term"
msgstr "ਤੁਹਾਡੇ ਖੋਜ ਸ਼ਬਦ ਨਾਲ ਮਿਲਦੀ ਕੋਈ ਐਪਲੀਕੇਸ਼ਨ ਜਾਂ ਈਵੈਂਟ ਹੈ"

#: ui/SourcesPage.qml:180
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr ""
"ਸੂਚੀ ਵਿੱਚੋਂ ਐਪਲੀਕੇਸ਼ਨ ਨੂੰ ਚੁਣੋ, ਜਿਸ ਦੀਆਂ ਨੋਟੀਫਿਕੇਸ਼ਨ ਸੈਟਿੰਗਾਂ ਅਤੇ ਰਵੱਈਏ ਦੀ ਸੰਰਚਨਾ ਕਰਨੀ ਚਾਹੁੰਦੇ ਹੋ"

#, fuzzy
#~| msgid "Application Settings"
#~ msgctxt "@action:button Application-specific notifications"
#~ msgid "Configure Application Settings…"
#~ msgstr "ਐਪਲੀਕੇਸ਼ਨ ਸੈਟਿੰਗਾਂ"

#~ msgctxt "@title:group"
#~ msgid "Application-specific settings"
#~ msgstr "ਖਾਸ ਐਪਲੀਕੇਸ਼ਨ ਸੈਟਿੰਗਾਂ"

#~ msgid "Configure…"
#~ msgstr "…ਸੰਰਚਨਾ"

#~ msgctxt "Enable Do Not Disturb mode when screens are mirrored"
#~ msgid "Enable:"
#~ msgstr "ਸਮਰੱਥ:"

#~ msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
#~ msgid "Keyboard shortcut:"
#~ msgstr "ਕੀਬੋਰਡ ਸ਼ਾਰਟਕੱਟ:"

#~ msgid "Configure Notifications"
#~ msgstr "ਸੂਚਨਾਵਾਂ ਲਈ ਸੰਰਚਨਾ"

#~ msgid "This module lets you manage application and system notifications."
#~ msgstr "ਇਹ ਮੋਡੀਊਲ ਤੁਹਾਨੂੰ ਐਪਲੀਕੇਸ਼ਨ ਅਤੇ ਸਿਸਟਮ ਨੋਟੀਫਿਕੇਸ਼ਨਾਂ ਦੇ ਇੰਤਜ਼ਾਮ ਕਰਨ ਲਈ ਸਹਾਇਕ ਹੈ।"

#~ msgctxt "Turn do not disturb mode on/off with keyboard shortcut"
#~ msgid "Toggle with:"
#~ msgstr "ਇਸ ਨਾਲ ਬਦਲੋ:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alam.yellow@gmail.com"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "ਕੋਈ ਉਵੇ ਬਰੋਉਲਿਕ"

#~ msgid "Show critical notifications"
#~ msgstr "ਗੰਭੀਰ ਨੋਟੀਫਿਕੇਸ਼ਨ ਵੇਖਾਓ"

#~ msgid "Always keep on top"
#~ msgstr "ਹਮੇਸ਼ਾ ਸਭ ਤੋਂ ਉੱਤੇ ਰੱਖੋ"

#~ msgid "Popup position:"
#~ msgstr "ਪੋਪਅੱਪ ਸਥਿਤੀ:"
