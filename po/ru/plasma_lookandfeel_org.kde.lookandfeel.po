# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# SPDX-FileCopyrightText: 2018, 2019, 2020, 2021, 2022, 2023, 2024 Alexander Yavorsky <kekcuha@gmail.com>
# Alexander Potashev <aspotashev@gmail.com>, 2014, 2015, 2016, 2017, 2018, 2019.
# Sidlovsky Yaroslav <zawertun@gmail.com>, 2019.
# Olesya Gerasimenko <translation-team@basealt.ru>, 2022.
# Alexander Yavorsky <kekcuha@gmail.com>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-02-18 21:39+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Раскладка клавиатуры: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Имя пользователя"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Пароль"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Войти"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "Включён режим Caps Lock"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Сон"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "Перезагрузить"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "Выключить"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Другое..."

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "Введите имя пользователя и пароль"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "Список пользователей"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Виртуальная клавиатура"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "Не удалось войти в систему"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Графический сеанс: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Часы:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Показывать при скрытии строки ввода пароля"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Элементы управления проигрывателем:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Показывать под строкой ввода пароля"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Не удалось разблокировать"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "Сон"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "Гибернация"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "Смена пользователя"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Сменить раскладку клавиатуры"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Разблокировать"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(или приложите палец к считывателю отпечатков)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(или используйте смарт-карту)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Без названия"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Ничего не воспроизводится"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Предыдущая дорожка"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Начать или приостановить воспроизведение"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Следующая дорожка"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "Установка обновлений и перезагрузка через %1 секунду"
msgstr[1] "Установка обновлений и перезагрузка через %1 секунды"
msgstr[2] "Установка обновлений и перезагрузка через %1 секунд"
msgstr[3] "Установка обновлений и перезагрузка через одну секунду"

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Перезагрузка через %1 секунду"
msgstr[1] "Перезагрузка через %1 секунды"
msgstr[2] "Перезагрузка через %1 секунд"
msgstr[3] "Перезагрузка через %1 секунду"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Завершение сеанса через %1 секунду"
msgstr[1] "Завершение сеанса через %1 секунды"
msgstr[2] "Завершение сеанса через %1 секунд"
msgstr[3] "Завершение сеанса через %1 секунду"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Выключение компьютера через %1 секунду"
msgstr[1] "Выключение компьютера через %1 секунды"
msgstr[2] "Выключение компьютера через %1 секунд"
msgstr[3] "Выключение компьютера через %1 секунду"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"В системе активны сеансы работы %1 другого пользователя. При выключении или "
"перезагрузке компьютера они могут потерять результаты своей работы."
msgstr[1] ""
"В системе активны сеансы работы %1 других пользователей. При выключении или "
"перезагрузке компьютера они могут потерять результаты своей работы."
msgstr[2] ""
"В системе активны сеансы работы %1 других пользователей. При выключении или "
"перезагрузке компьютера они могут потерять результаты своей работы."
msgstr[3] ""
"В системе активен сеанс работы другого пользователя. При выключении или "
"перезагрузке компьютера он может потерять результаты своей работы."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr "При перезагрузке компьютера будет показан экран настройки прошивки."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "Ждущий режим"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "Спящий режим"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Установить обновления и перезагрузить"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "Установить обновления и перезагрузить"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "Перезагрузить"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "Выключение"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "Завершить сеанс"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "Завершить сеанс"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "Отмена"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1 %"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "KDE Plasma"

#~ msgid "OK"
#~ msgstr "ОК"

#~ msgid "Switch to This Session"
#~ msgstr "Переключить в этот сеанс"

#~ msgid "Start New Session"
#~ msgstr "Начать новый сеанс"

#~ msgid "Back"
#~ msgstr "Назад"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "Заряд батареи %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Не используется"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "Терминал %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "на терминале %1 (экран %2)"

# BUGME: this is bad syntax: "myusername (on TTY 7 (Display 0))" --aspotashev
#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100 %"

#~ msgid "Configure"
#~ msgstr "Настроить"

#~ msgid "Configure KRunner Behavior"
#~ msgstr "Настроить строку поиска и запуска KRunner"

#~ msgid "Configure KRunner…"
#~ msgstr "Настроить строку поиска и запуска..."

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Поиск... (%1)"

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Поиск..."

#~ msgid "Show Usage Help"
#~ msgstr "Показать справку по использованию"

#~ msgid "Pin"
#~ msgstr "PIN"

#~ msgid "Pin Search"
#~ msgstr "Поиск PIN"

#~ msgid "Keep Open"
#~ msgstr "Держать открытым"

#~ msgid "Recent Queries"
#~ msgstr "Последние запросы"

# BUGME: please clarify context --aspotashev
#~ msgid "Remove"
#~ msgstr "Удалить"

#~ msgid "in category recent queries"
#~ msgstr "в категории «Последние запросы»"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "Показывать:"

#~ msgid "Configure Search Plugins"
#~ msgstr "Настроить строку поиска и запуска"

#~ msgctxt ""
#~ "This is the first text the user sees while starting in the splash screen, "
#~ "should be translated as something short, is a form that can be seen on a "
#~ "product. Plasma is the project name so shouldn't be translated."
#~ msgid "Plasma 25th Anniversary Edition by KDE"
#~ msgstr "Юбилейный выпуск Plasma на 25-летие KDE"

#~ msgid "Close"
#~ msgstr "Закрыть"

#~ msgctxt "verb, to show something"
#~ msgid "Always show"
#~ msgstr "Показывать всегда"

#~ msgid "Suspend"
#~ msgstr "Ждущий режим"

#~ msgid "Reboot"
#~ msgstr "Перезагрузить"

#~ msgid "Logout"
#~ msgstr "Завершить сеанс"

# BUGME: I can't translate this without context --aspotashev
#~ msgid "Different User"
#~ msgstr "Выбрать пользователя"

#~ msgid "Log in as a different user"
#~ msgstr "Вход с выбором пользователя"

#, fuzzy
#~ msgid "Password..."
#~ msgstr "Пароль"

#~ msgid "Login"
#~ msgstr "Войти"

#~ msgid "Shutdown"
#~ msgstr "Выключить"

#~ msgid "Switch"
#~ msgstr "Переключить"

#~ msgid "New Session"
#~ msgstr "Новый сеанс"

#~ msgid "%1%. Charging"
#~ msgstr "%1%, заряжается"

#~ msgid "Fully charged"
#~ msgstr "Полностью заряжено"

#~ msgid "%1%. Plugged in, not charging"
#~ msgstr "%1%, подключено к сети, не заряжается"

#~ msgid "%1% battery remaining"
#~ msgstr "Осталось %1 % заряда"

#~ msgctxt "Button to restart the computer"
#~ msgid "Reboot"
#~ msgstr "Перезагрузить компьютер"

#~ msgctxt "Button to shut down the computer"
#~ msgid "Shut down"
#~ msgstr "Выключить компьютер"

#~ msgctxt "Dialog heading, confirm log out, not a status label"
#~ msgid "Logging out"
#~ msgstr "Завершение сеанса"

#~ msgctxt "Dialog heading, confirm reboot, not a status label"
#~ msgid "Rebooting"
#~ msgstr "Перезагрузка компьютера"

#~ msgid "Change Session"
#~ msgstr "Переключить пользователя"

#~ msgid "Change Session..."
#~ msgstr "Переключить пользователя..."

# BUGME: I can't translate this without context --aspotashev
#~ msgid "Search for User"
#~ msgstr "Поиск пользователя"

#~ msgid "There are currently no other active sessions."
#~ msgstr "Нет других запущенных сеансов."

#~ msgctxt "Username (logged in on console)"
#~ msgid "%1 (TTY)"
#~ msgstr "%1 (терминал)"

#~ msgctxt "Unused session, nobody logged in there"
#~ msgid "Unused"
#~ msgstr "Не используется"

#~ msgctxt "Username (on display number)"
#~ msgid "%1 (on Display %2)"
#~ msgstr "%1 (на экране %2)"

# BUGME: "myusername (0)" is not user-friendly --aspotashev
#~ msgctxt "Username (on display number)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"
