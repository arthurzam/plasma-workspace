# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2014, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 Vincenzo Reale <smart2128vr@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-01-31 08:50+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Mappatura della tastiera: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Nome utente"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Password"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Accedi"

#: ../sddm-theme/Main.qml:200 contents/lockscreen/LockScreenUi.qml:276
#, kde-format
msgid "Caps Lock is on"
msgstr "Il blocco maiuscole è attivo"

#: ../sddm-theme/Main.qml:212 ../sddm-theme/Main.qml:356
#: contents/logout/Logout.qml:200
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Sospendi"

#: ../sddm-theme/Main.qml:219 ../sddm-theme/Main.qml:363
#: contents/logout/Logout.qml:225 contents/logout/Logout.qml:238
#, kde-format
msgid "Restart"
msgstr "Riavvia"

#: ../sddm-theme/Main.qml:226 ../sddm-theme/Main.qml:370
#: contents/logout/Logout.qml:251
#, kde-format
msgid "Shut Down"
msgstr "Spegni"

#: ../sddm-theme/Main.qml:233
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Altro…"

#: ../sddm-theme/Main.qml:342
#, kde-format
msgid "Type in Username and Password"
msgstr "Digita nome utente e password"

#: ../sddm-theme/Main.qml:377
#, kde-format
msgid "List Users"
msgstr "Elenca utenti"

#: ../sddm-theme/Main.qml:452 contents/lockscreen/LockScreenUi.qml:363
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Tastiera virtuale"

#: ../sddm-theme/Main.qml:526
#, kde-format
msgid "Login Failed"
msgstr "Accesso non riuscito"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Sessione del desktop: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Orologio:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Mantieni visibile quando la richiesta di sblocco scompare"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Controlli multimediali:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Mostra sotto richiesta di sblocco"

#: contents/lockscreen/LockScreenUi.qml:50
#, kde-format
msgid "Unlocking failed"
msgstr "Sblocco non riuscito"

#: contents/lockscreen/LockScreenUi.qml:290
#, kde-format
msgid "Sleep"
msgstr "Sospendi"

#: contents/lockscreen/LockScreenUi.qml:296 contents/logout/Logout.qml:210
#, kde-format
msgid "Hibernate"
msgstr "Ibernazione"

#: contents/lockscreen/LockScreenUi.qml:302
#, kde-format
msgid "Switch User"
msgstr "Cambia utente"

#: contents/lockscreen/LockScreenUi.qml:387
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Cambia disposizione"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Sblocca"

#: contents/lockscreen/MainBlock.qml:156
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr "(o esegui la scansione dell'impronta sul tuo lettore)"

#: contents/lockscreen/MainBlock.qml:160
#, kde-format
msgid "(or scan your smartcard)"
msgstr "(o esegui la scansione della tua smartcard)"

#: contents/lockscreen/MediaControls.qml:59
#, kde-format
msgid "No title"
msgstr "Nessun titolo"

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No media playing"
msgstr "Nessun supporto in riproduzione"

#: contents/lockscreen/MediaControls.qml:88
#, kde-format
msgid "Previous track"
msgstr "Traccia precedente"

#: contents/lockscreen/MediaControls.qml:100
#, kde-format
msgid "Play or Pause media"
msgstr "Riproduci o sospendi supporto"

#: contents/lockscreen/MediaControls.qml:110
#, kde-format
msgid "Next track"
msgstr "Traccia successiva"

#: contents/logout/Logout.qml:151
#, kde-format
msgid "Installing software updates and restarting in 1 second"
msgid_plural "Installing software updates and restarting in %1 seconds"
msgstr[0] "Installazione degli aggiornamenti software e riavvio tra 1 secondo"
msgstr[1] "Installazione degli aggiornamenti software e riavvio tra %1 secondi"

#: contents/logout/Logout.qml:152
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Riavvio tra 1 secondo"
msgstr[1] "Riavvio tra %1 secondi"

#: contents/logout/Logout.qml:154
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Chiusura della sessione in 1 secondo"
msgstr[1] "Chiusura della sessione in %1 secondi"

#: contents/logout/Logout.qml:157
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Spegnimento in 1 secondo"
msgstr[1] "Spegnimento in %1 secondi"

#: contents/logout/Logout.qml:172
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Un altro utente ha attualmente eseguito l'accesso. Se il computer viene "
"spento o riavviato, l'utente potrebbe perdere il suo lavoro."
msgstr[1] ""
"%1 altri utenti hanno attualmente eseguito l'accesso. Se il computer viene "
"spento o riavviato, tali utenti potrebbero perdere il loro lavoro."

#: contents/logout/Logout.qml:187
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr ""
"Se riavviato, il computer accederà alla schermata di configurazione del "
"firmware."

#: contents/logout/Logout.qml:201
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep Now"
msgstr "Sospendi subito"

#: contents/logout/Logout.qml:211
#, kde-format
msgid "Hibernate Now"
msgstr "Iberna subito"

#: contents/logout/Logout.qml:222
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart"
msgstr "Installa gli aggiornamenti e riavvia"

#: contents/logout/Logout.qml:223
#, kde-format
msgctxt "@action:button Keep short"
msgid "Install Updates & Restart Now"
msgstr "Installa gli aggiornamenti e riavvia subito"

#: contents/logout/Logout.qml:226 contents/logout/Logout.qml:239
#, kde-format
msgid "Restart Now"
msgstr "Riavvia subito"

#: contents/logout/Logout.qml:252
#, kde-format
msgid "Shut Down Now"
msgstr "Spegni subito"

#: contents/logout/Logout.qml:262
#, kde-format
msgid "Log Out"
msgstr "Chiudi sessione"

#: contents/logout/Logout.qml:263
#, kde-format
msgid "Log Out Now"
msgstr "Chiudi sessione subito"

#: contents/logout/Logout.qml:273
#, kde-format
msgid "Cancel"
msgstr "Annulla"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma creato da KDE"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Switch to This Session"
#~ msgstr "Passa a questa sessione"

#~ msgid "Start New Session"
#~ msgstr "Avvia nuova sessione"

#~ msgid "Back"
#~ msgstr "Indietro"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "Batteria al %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Inutilizzata"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "su TTY %1 (%2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "Configure"
#~ msgstr "Configura"

#~ msgid "Configure KRunner Behavior"
#~ msgstr "Configura il comportamento di KRunner"

#~ msgid "Configure KRunner…"
#~ msgstr "Configura KRunner…"

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Cerca «%1»..."

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Cerca..."

#~ msgid "Show Usage Help"
#~ msgstr "Mostra la guida di utilizzo"

#~ msgid "Pin"
#~ msgstr "Appunta"

#~ msgid "Pin Search"
#~ msgstr "Appunta ricerca"

#~ msgid "Keep Open"
#~ msgstr "Mantieni aperto"

#~ msgid "Recent Queries"
#~ msgstr "Interrogazioni recenti"

#~ msgid "Remove"
#~ msgstr "Rimuovi"

#~ msgid "in category recent queries"
#~ msgstr "nella categoria interrogazioni recenti"

#~ msgctxt "verb, to show something"
#~ msgid "Show:"
#~ msgstr "Mostra:"

#~ msgid "Configure Search Plugins"
#~ msgstr "Configura estensioni di ricerca"

#~ msgctxt ""
#~ "This is the first text the user sees while starting in the splash screen, "
#~ "should be translated as something short, is a form that can be seen on a "
#~ "product. Plasma is the project name so shouldn't be translated."
#~ msgid "Plasma 25th Anniversary Edition by KDE"
#~ msgstr "Edizione 25° anniversario di Plasma di KDE"

#~ msgid "Close"
#~ msgstr "Chiudi"

#~ msgctxt "verb, to show something"
#~ msgid "Show always"
#~ msgstr "Mostra sempre"

#~ msgctxt "verb, to show something"
#~ msgid "Always show"
#~ msgstr "Mostra sempre"

#~ msgid "Suspend"
#~ msgstr "Sospendi"

#~ msgid "Reboot"
#~ msgstr "Riavvia"

#~ msgid "Logout"
#~ msgstr "Chiudi sessione"

#~ msgid "Type User"
#~ msgstr "Digita utente"

#~ msgid "Different User"
#~ msgstr "Altro utente"

#~ msgid "Log in as a different user"
#~ msgstr "Accedi con un altro utente"

#~ msgid "Password..."
#~ msgstr "Password..."

#~ msgid "Login"
#~ msgstr "Accesso"

#~ msgid "Shutdown"
#~ msgstr "Spegni"

#~ msgid "Switch"
#~ msgstr "Cambia"

#~ msgid "New Session"
#~ msgstr "Nuova sessione"

#~ msgid "%1%. Charging"
#~ msgstr "%1%. In ricarica"

#~ msgid "Fully charged"
#~ msgstr "Caricata completamente"

#~ msgid "%1%. Plugged in, not charging"
#~ msgstr "%1%. Collegato, non in carica"

#~ msgid "%1% battery remaining"
#~ msgstr "%1% batteria rimanente"

#~ msgid "Change Session"
#~ msgstr "Cambia sessione"

#~ msgid "Create Session"
#~ msgstr "Crea sessione"

#~ msgid "Change Session..."
#~ msgstr "Cambia sessione..."

#~ msgid "There are currently no other active sessions."
#~ msgstr "Non ci sono attualmente altre sessioni attive."

#~ msgctxt "Username (logged in on console)"
#~ msgid "%1 (TTY)"
#~ msgstr "%1 (TTY)"

#~ msgctxt "Unused session, nobody logged in there"
#~ msgid "Unused"
#~ msgstr "Inutilizzata"

#~ msgctxt "Username (on display number)"
#~ msgid "%1 (on Display %2)"
#~ msgstr "%1 (su Display %2)"

#~ msgctxt "Button to restart the computer"
#~ msgid "Reboot"
#~ msgstr "Riavvia"

#~ msgctxt "Button to shut down the computer"
#~ msgid "Shut down"
#~ msgstr "Spegni"

#~ msgctxt "Dialog heading, confirm log out, not a status label"
#~ msgid "Logging out"
#~ msgstr "Chiusura della sessione"

#~ msgctxt "Dialog heading, confirm reboot, not a status label"
#~ msgid "Rebooting"
#~ msgstr "Riavvio"

#~ msgid "Search for User"
#~ msgstr "Cerca per utente"

#~ msgctxt "Username (on display number)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"
